class Clients{
    constructor(){
        this.clientlist = {};
            
    }

    saveCLient(username, client){
        this.clientlist[username] = client;

    }
}
const clients = new Clients();

const express = require('express');
const http = require('http');
const WebSocket = require('ws');

const port = 8080;
const server = http.createServer(express);
const wss = new WebSocket.WebSocketServer({server});


wss.on('connection',(ws,req)=>{
    const ip = req.socket.remoteAddress;
    var sysmsg;
    var msg;  
    sysmsg = {
        'type':'system',
        'channel':'all',
        'send_from':'--- server ---', 
        'msg':"Sent from server: Connected Successfully",
        'send_to':'',
        'ip':''       
    };
    sysmsg = JSON.stringify(sysmsg);

    ws.send(sysmsg);
    console.log("Total Clients: " +  wss.clients.size);


    ws.on("message", (data,isBinary)=>{
        data = JSON.parse(data);
        console.log(data);

        msg = {
            'type':data.type,
            'channel':data.channel,
            'send_from':data.uid, 
            'msg':data.msg,
            'send_to':data.send_to,
            'ip':ip,            
        };
       msg = JSON.stringify(msg);
       console.log(msg);

        



        if(data.type == "chat"){   
                if(data.channel == 'all'){
        wss.clients.forEach(function each(client){
            if(client !== ws && client.readyState == WebSocket.OPEN){
            //     const ws_to = clients.clientlist[data.uid]['ws'];
            // console.log(ws_to);
                client.send(msg, { binary: isBinary });
            }
        });
            }else if(data.channel == "individual"){
                const ws_to = clients.clientlist[data.send_to]['ws'];
                ws_to.send(msg, { binary: isBinary });
            }



        }else if(data.type == "system"){
                const parsedMsg = data.msg;
                const userData = {
                    'ws' : ws,
                    'ipaddress' : ip
                }
                clients.saveCLient(data.uid,userData)
                console.log(clients);
        }
    });
    ws.on("error", (data)=>{
        console.log('sss');
    });
       
});

server.listen(port, function(){
console.log("Server is listending on " + port);
});

