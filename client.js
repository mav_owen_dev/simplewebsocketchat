const http = require('http')
const fs = require('fs')

const server = http.createServer((req, res) => {

    fs.readFile("./" + req.url,function(error,data){ 
		if(error){ 
			res.writeHead(404,{"Content-type":"text/plain"}); 
			res.end("Sorry the page was not found"); 
		}else{ 
			res.writeHead(202,{"Content-type":"text/html"}); 
			res.end(data); 
		} 

//   res.writeHead(200, { 'content-type': 'text/html' })
//   fs.createReadStream('index.html').pipe(res)
})
});

const port=3000;
server.listen(port,"0.0.0.0", function(){
    console.log("Client is listending on " + port);
    });
